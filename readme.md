# Télétravail, mythes et légendes

Retour d'experience à propos du télétravail, le bon et le moins bon.

Talk présenté lors de l'[APIHour#18](http://clermontech.org/api-hours/api-hour-18.html)
organisé par [Clermont'ech](http://clermontech.org/)

Voir les [slides](http://krichprollsch.gitlab.io/teletravail/index.html)

## Installation

```
git clone
git submodule update --init
```

## Démarrage via docker & php


Installer [Docker](https://docs.docker.com/engine/installation/) puis lancer

```
docker run --rm -p 80:80 -v $PWD:/srv php:5.6-cli php -S 0.0.0.0:80 -t /srv
```
