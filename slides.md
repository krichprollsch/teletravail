### Mythes et réalités du
# Télétravail


### Pierre Tachoire

Développeur backend [@wisembly](http://wisembly.com/) + [@Get_Solid](http://getsolid.io/)

Télétravailleur

[@krichprollsch](https://twitter.com/krichprollsch)


### Disclaimer

> Cette présentation n'est pas une présentation sur le cyclimse


## Movies vs Life

---

## In the movies


<a data-flickr-embed="true"  href="https://www.flickr.com/photos/kovicfotos/2367554278/in/photolist-4Bdkpm-7qC3qG-9XSvNf-9Ycett-JFZBQ-dCJJy6-9Yc8Hx-qWk8Zb-9Yc1Cz-9Ybb6D-9YbjZ4-rdLmS2-mpKR7C-9YdK6d-9YbisV-9YecpU-9YdWD5-9Yb4Vg-9YaZa2-7pq5xP-nKEvvp-aZddz-4n9hLu-9Ybu3x-9YeBs5-9Yb5AX-9YrY3x-9YdRuC-9YcdNt-9YuNVL-9YerEh-9YbvBX-9Yc12B-9YaPkK-9YuHUN-nRJitY-o18Z6L-8bMUo1-yh2Kq-9Yf9uN-4qfK7o-9YdX9j-jMgvyQ-ddQpY-3ftGGL-7DMitn-95KdaZ-bkp5vV-jZdvxb-nHJmB" title="llamando"><img src="https://farm4.staticflickr.com/3275/2367554278_545f1dc973_z.jpg?zz=1" width="640" height="480" alt="llamando"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

> Cool! Tu peux bosser en slip.

Note: Ce qui est cool en télétravail c'est que tu peux bosser en slip!


## In life


- S'habiller comme si on était vraiment au bureau
- Avoir le bon **état d'esprit** pour travailler
- Être *au travail* et pas entre deux tâches personnelles

---

## In the movies


<a data-flickr-embed="true"  href="https://www.flickr.com/photos/xelcise/5400445529/in/photolist-9edGiP-7VpjHQ-4HtwqB-gixU6U-pUx1kj-8qyWFJ-Pcd7G-pfmdpz-pUFZv4-7Hnwcs-7z1h6F-q9PPou-pUEJ2T-pRF8Gf-9zPNEx-q6XPnQ-pcujND-3eYsce-NXUaJ-pUG1ZX-pRQ4Ki-pRQ568-4EMbzZ-p4h72-sNbv-59oGzP-gixYMt-gixTaW-giyuRp-pRNQYF-7fWXG4-amzYSg-9W2MhA-pcfWeY-fKf2B-4qHGiw-qc73LX-5q4ELh-e2H9G-9ZGea5-gixWcj-gixS6y-gixU9a-gixsRL-cs6CMh-cs6zzb-4qHGis-qc778r-dgeuHJ-9KaFcu" title="27/365"><img src="https://farm6.staticflickr.com/5096/5400445529_38e41aedc7_z.jpg" width="640" height="425" alt="27/365"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

> Tu peux bosser quand tu veux !


## In life


- Respecter les **horaires de l'entreprise**, c'est faire partie de l'entreprise
- Travail en équipe = travailler au même moment
- Les autres collègues doivent **savoir quand vous êtes disponible**


- On ne vous voit pas !
- Ne frustrez pas vos collègues
- **Annoncez** vos arrivées, vos departs et vos pauses


<img src="images/deploy.png" alt="deploying" />

---

## In the movies


<a data-flickr-embed="true"  href="https://www.flickr.com/photos/buffaloted/2912824644/in/photolist-rBaoST-vsPbej-6hFGjK-5cuH4u-5CCGSG-9D5Geo-MuMut-4JzaGt-5D8yYZ-dmMSx6-a9Vu7d-fsHC8A-36LaMU-3uXZB7-5roZhy-DWcrp-o7tJuN-84KX6U-9juhyH-pkX7ya-dz5QR-8dRkfv-5RMt1-fQLTS9-e1TTP1-a5RPnB-5pQ7Ja-5YwT1S-qwbGj-3cn9Na-9qCPmc-mkBArL-5oPgTR-BjjtRg-dDpMBr-dwD3Ur-z5VGdm-6PGekN-oc9CUN-SWVX-7MZZ3W-7MVZct-ztX78-efJLcx-anutiY-aqD433-nZSWAN-66JqbR-inrhw-6WgB8" title="Hard at work"><img src="https://farm4.staticflickr.com/3239/2912824644_1b0b75cff2_z.jpg" width="640" height="512" alt="Hard at work"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

> Tu peux te concentrer sans être dérangé !


## In life


<a data-flickr-embed="true"  href="https://www.flickr.com/photos/ileohidalgo/22486951310/in/photolist-Ag6vc5-6Ykwcd-4q2Bq-hVK653-9yxvfq-5v3Lt6-g8n228-9T1q6q-pFXLdv-5WvzqW-pnGSkm-Crqh1s-anfxgV-qVWeFg-8P9Em-4bD6Eq-8L977R-qXKs9N-5zF3UE-zxxC7J-e5aMo2-7NuE5Y-pzSaqd-5dteLq-gJafVr-7xa3pR-fKYPaU-5eyoDN-4SCae-oSHavX-rok3H4-y8PYmF-F1o7-5hpazs-9MJc6S-bo8yHi-b6CpRR-o43fDT-hPJPnr-a1cYBZ-5HgmXD-W8ALu-j4cRRp-e8UGXQ-9AdVbA-pwZN3-hxMjDf-Cj981o-7aQ2w2-7jyCi1" title="Hard working"><img src="https://farm6.staticflickr.com/5823/22486951310_7b78a07717_z.jpg" width="640" height="427" alt="Hard working"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

Attention au sentiment de **solitude**


- Garder un **lien fort** avec ses collègues
- Ne pas oublier la **communication informelle**


<img src="images/appear.png" alt="Voir les bureaux" />

---

## In the movies


<a data-flickr-embed="true"  href="https://www.flickr.com/photos/citrixonline/5446645129/in/photolist-9iitQD-8Hs47P-8QjVp-5WfB6Z-51Xspj-5UqWg5-733YSC-Apmx4U-e5Uft3-8n9uuJ-dpdnMa-6Jngej-cw9u1d-drxTq3-5sXB5Z-aySvKu-dYoY89-2hnWQz-7vu8w1-brRknD-4j79Bx-kzcSo7-fVLRBG-8D3CAS-aJmNr6-6MEb6U-eL5yS8-8P9AZr-9hdCaX-6DMxXM-Gxij-brrrhX-azQmTj-qTdZ33-65wJFt-9iitVk-7mBRZH-9imzk5-5f25r6-9warQX-ez1AGr-6rHBLK-4ycBG4-dpdwSG-9iiutV-6bUpPC-3TB3hx-9imzS3-6NM8Qe-CRmVV" title="Workshifting: Amber, Senior Events Specialist"><img src="https://farm6.staticflickr.com/5011/5446645129_59245d10f7_z.jpg" width="640" height="428" alt="Workshifting: Amber, Senior Events Specialist"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

> Tu peux bosser d'où tu veux


## In life


![](images/chat.png)

> Un bureau c'est mieux


- Voir des gens c'est aussi important (co-working, ...)
- N'**enfermez pas le télétravailleur** chez lui


Utilisez des outils qui fonctionnent **en local comme à distance**

---

## In the movies


<a data-flickr-embed="true"  href="https://www.flickr.com/photos/nalbertini/6806221795/in/photolist-bnrEBD-82ayUG-fUwsoZ-4fqarD-6Ay2eV-eefEM6-4iY3nP-i9iaKP-5Zpy6-6FGxXS-5pyEqD-rxhaD2-6beD4e-5pCXus-7vgRR3-7HnfuK-Y4KHc-anbUwx-j1MD2-che8mw-ZRrBH-cwHuHf-anbTWF-mXmSB6-4pbCLN-cwHhZW-nZZjC2-rJqaP6-qJDfDN-6p8QSp-2E3QH8-b5Mi6-3Jjyq8-5oMEMG-5cTnAW-jNfhpk-5QqXG-dvRi7B-9W3vub-4LUHTd-anxUKa-5cP4Lk-iQgDr2-5cTntQ-C6PtV-7FXUKZ-yX84F6-2EjNqw-5cTnTo-9z6WJE" title="Run before you get vacuumed"><img src="https://farm8.staticflickr.com/7025/6806221795_c93480a70a_z.jpg" width="640" height="424" alt="Run before you get vacuumed"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

> Le télétravailleur fait son ménage au lieu de bosser.


## In life


Le télétravail est basé sur **la confiance**.



- La vérification du travail est **identique** avec tous les employés
- Inutile de demander plus de justifications à un télétravailleur


Il faut produire **la même transparence** que si on était dans un bureau


Tous les employés peuvent faire du **télétravail ponctuel** pour se rendre compte

---

## In the movies


<a data-flickr-embed="true"  href="https://www.flickr.com/photos/ventriloblog/144586940/in/photolist-6JZADQ-mZtCWo-8jGX3w-8Rre1-qCJV9T-gwjrE-5yViti-8zsnc8-ifkPM-38WHyr-5ewFAy-35WQgm-9DMzV4-cDTDHS-4Q7YcU-7V8CxY-ef9NAJ-wWy687-7EBnpg-7dar3o-akicg5-2j7DVw-bGJUk-qkvwv-4HxoZ6-dM3Eh-pJeQ4c-6EGk6D-bpqDzj-9DQszu-ef45Az-ef9PNw-Mk96K-jEBhnH-jEEED8-p512GL-cGCqn-BrzKuE-boi4Fs-4yip6W-ozzNF-6zHXbB-8gJ9R7-7FNE9H-fTxy2-6kmHwE-522oaS-5sMQvJ-6zN4gQ-e1FZwk" title="Completed Weird-Oh"><img src="https://farm1.staticflickr.com/51/144586940_3ad87c10a4_n.jpg" width="240" height="320" alt="Completed Weird-Oh"></a><script async src="//embedr.flickr.com/assets/client-code.js" charset="utf-8"></script>

> Les télétravailleurs sont difficiles à intégrer dans une équipe


## In life


- Prendre soin d'**embarquer** les télétravailleurs
- **Se voir régulièrement** et définir dès le début un rythme


- 4 premiers mois en forte présence au bureau
- Présence 2 jours tous les 15 jours en croisière
- Vidéo-diffusion de **toutes les réunions**


<img src="images/halloween.png" alt="haloween" />

---

### Conclusion

## Faites du télétravail !

---

- [Slack](https://slack.com), [Hipchat](https://www.hipchat.com/), [Mattermost](http://www.mattermost.org/), [Rocket.chat](https://rocket.chat/)
- [Appear.in](https://appear.in), [Talky.io](https://talky.io), [Firefox Hello](https://www.mozilla.org/fr/firefox/hello/)
- [Trello](https://trello.com)
- [Github](https://github.com), [Gitlab](https://gitlab.com/)
